require('dotenv').config();
const express = require('express');
const { userRoutes } = require('./routes/');
require('./database');

const app = express();

app.set('port', process.env.PORT || 5000);

app.use(express.json()).use(express.urlencoded({extended: false}));
app.use(userRoutes);

app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});
