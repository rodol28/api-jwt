const mongoose = require('mongoose');
const config = require('./config/config').get(process.env.NODE_ENV);

mongoose.connect(config.DATABASE, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(db => console.log('db is conected'))
    .catch(err => console.error(new Error('Failed to connect to database')));