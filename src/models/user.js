const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/config').get(process.env.NODE_ENV);

const userSchema = new Schema({
    firstname: {
        type: String,
        required: true,
        maxlength: 100
    },
    lastname: {
        type: String,
        required: true,
        maxlength: 100
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    password2: {
        type: String,
        required: true,
        minlength: 8
    },
    token: {
        type: String
    }
});

// This function is used for hashing the user password, 
// so when we save our user into the database this function 
// will call itself and hashed our password
userSchema.pre('save', function(next) {
    let user = this;

    console.log(user);
    if(user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            if(err) return next(new Error('Error in genSalt'));

            bcrypt.hash(user.password, salt, (err, hash) => {
                if(err) return next(new Error('Error in hash'));
                user.password = hash;
                user.password2 = hash;
                next();
            })
        });
    } else { 
        next();
    }
});

// // Here, we have checked whether passwords are same or not.
// userSchema.methods.comparePassword = (password, callback) => {
//     bcrypt.compare(password, this.password, (err, isMatch) => {
//         if(err) return callback(new Error('Error compare password'));
//         callback(null, isMatch);
//     });
// };

// // Next function is for generating a token when user logged in.
// userSchema.methods.generateToken = (callback) => {
//     let user = this;
//     let token = jwt.sign(user._id.toHexString(), config.SECRET);

//     user.token = token;
//     user.save((err, user) => {
//         if(err) return callback(err);
//         callback(null, user);
//     });
// };

// // Next method is to find a particular token
// // This method is used when we have to find 
// // whether a user is logged-in or not
// userSchema.statics.findByToken = (token, callback) => {
//     let user = this;

//     jwt.verify(token, config.SECRET, (err, decode) => {
//         user.findOne({'_id': decode, 'token': token}, (err, user) => {
//             if(err) return callback(err);
//             callback(null, user);
//         })
//     });
// };

// // next method is for deleting a token, when the 
// // user logout we will delete this particular token.
// userSchema.methods.deleteToken = (token, callback) => {
//     let user = this;

//     user.update({$unset: {token: 1}}, (err, user) => {
//         if(err) return callback(err);
//         callback(null, user); 
//     });
// }

module.exports = model('User', userSchema);