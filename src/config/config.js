const config = {
    production: {
        SECRET: process.env.SECRET,
        DATABASE: process.env.MONGO_URI_PRO
    },
    default: {
        SECRET: process.env.SECRET,
        DATABASE: process.env.MONGO_URI_DEV
    }
}

exports.get = function get(env) {
    return config[env] || config.default; 
}