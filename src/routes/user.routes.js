const router = require('express').Router();
const { User } = require('../models');
const { auth } = require('../middlwares/auth');

router.get('/api', (req, res) => {
    res.status(200).send('Welcome to login, sign-up api');
});

// create new user (sign-up route)
router.post('/api/register', async (req, res) => {

    const newUser = new User(req.body);
    
    if(newUser.password != newUser.password2) { 
        return res.status(400).json({
            message: 'password not match.'
        });
    }
    
    const user = await User.findOne({email: newUser.email});
    
    if(user) {
        return res.status(400).json({auth: false, message: 'email exits'})
    }

    try {
        console.log(newUser);
        const userSaved = await newUser.save();
        res.status(200).json({
            success: true,
            user: userSaved
        });
    } catch (error) {
        console.error(new Error('Save error'));
        return res.status(400).json({success: false});
    } 

});

module.exports = router;