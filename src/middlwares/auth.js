const { User } = require('../models');

let auth = (req, res, next) => {
    let token = req.authtoken;
    User.findByToken(token, (err, user) => {
        if(err) return next(err);
        if(!user) return res.status(401).json({
            message: 'User not logged in.',
            error: true
        });

        req.token = token;
        req.user = user;
        next();
    });
}

module.exports = { auth };